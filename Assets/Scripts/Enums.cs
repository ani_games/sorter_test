﻿//Enums

//GameStates
public enum GameState
{
    None,
    Menu,
    StartRound,
    Result
}

//ScreenStates
public enum ScreenState
{
    MainMenu,
    PlayMenu
}

//Box Form
public enum BoxForm
{
    None,
    Cube,
    Sphere
}

//Box Color
public enum BoxColor
{
    None,
    Black,
    White,
    Green,
    Blue,
    Red
}

public enum BoxType
{
    None,
    SimpleBox,
    ChristmasGift,
    MilkBox,
    WeaponBox
}

public enum ControlDirection
{
    None,
    Right,
    Left
}

public enum CollectPlace
{
    A,
    B,
    C,
    D,
    None
}