﻿using UnityEngine;
using System.Collections;


public class LevelParameters : ScriptableObject
{

    [Header("Появление")]
    [SerializeField]
    [Range(0f, 1f)]
    [Tooltip("Шанс выпадения данного Уровня")]
    float chance;
    [SerializeField]
    [Tooltip("Число коробок, необходимое собрать, чтоб перейти на новый уровень")]
    int maxCount;
    [SerializeField]
    [Range(0f, 1f)]
    [Tooltip("Скорость движения конвеера")]
    float transporterSpeed = 0.5f;
    [SerializeField]
    [Range(0f, 1f)]
    [Tooltip("Частота появления коробки. 0 - одна коробка на конвеере, 1 - все коробки, которые помещаются, 0.5 - половина от всех, которые могут уместиться")]
    float frequency = 0.5f;
    [SerializeField]
    [Tooltip("Ширина коробки в Юнитах. На начало работы над проектом одна коробка по умолчанию занимает 2 юнита.")]
    float boxScale = 2;

   
    [SerializeField]
    [Tooltip("Массив всех коробок (Настроить и указать Префаб)")]
    BoxParameters[] boxes;

    /// <summary>
    /// Шанс выбора данного уровня
    /// </summary>
    public float Chance { get { return chance; } }
    /// <summary>
    /// Макимальное число коробок, которое необходимо собрать для перехода на новый уровень
    /// </summary>
    public int MaxCount { get { return maxCount; } }
    /// <summary>
    /// Массив всех типов коробок на данном уровне
    /// </summary>
    public BoxParameters[] Boxes { get
        {
            BoxParameters[] pars = new BoxParameters[boxes.Length];
            boxes.CopyTo(pars, 0);
            return pars;
        }  }
    /// <summary>
    /// Частота появления коробок. Значение от 0 до 1
    /// </summary>
    public float Frequency { get { return frequency; } }
    /// <summary>
    /// Ширина коробки. Необходимо для определения минимального расстояния между двумя коробками.
    /// </summary>
    public float BoxScale { get { return boxScale; } }

    /// <summary>
    /// Возвращает скорость движения конвеера от 0 до 1 
    /// </summary>
    public float TransporterSpeed
    {
        get
        {
            return transporterSpeed;
        }
    }

}
