﻿using UnityEngine;
using System.Collections;

public class HouseSettings : ScriptableObject
{

    [SerializeField]
    Vector3 boxStartPosition, boxEndPosition;

    [SerializeField]
    float startCollectDistance;

    [SerializeField]
    float lightIntensity = 1;

    public Vector3 BoxStartPosition { get { return boxStartPosition; } }
    public Vector3 BoxEndPosition { get { return boxEndPosition; } }
    public float StartCollectDistance { get { return startCollectDistance; } }
    public float LightIntensity { get { return lightIntensity; } }


}
