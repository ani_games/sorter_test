﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BoxParameters
{

    [Header("Появление")]
    [SerializeField]
    [Range(0f, 1f)]
    float chance;
    [SerializeField]
    [Range(0f, 1f)]
    float fillRate;
    [SerializeField]
    int attempt;

    [Header("Уникальный ИД коробки")]
    [SerializeField]
    public string UniqueID;

    [Header("Префаб")]
    [SerializeField]
    GameObject prefab;

    public float Chance { get { return chance; } }
    public float FillRate { get { return fillRate; } }
    public int Attempt { get { return attempt; } }
    public GameObject Prefab { get { return prefab; } }

}
