﻿using UnityEngine;
using System.Collections;

public class PathManager : ScriptableObject
{

    [SerializeField]
    public string HousesPath = "";

    [SerializeField]
    [Header("имя файла HouseSettings_ + id персонажа")]
    public string HousesSettingsPath = "";

    [SerializeField]
    [Header("имя файла Complication_ + id персонажа")]
    public string ComplicationPath = "";

}
