﻿using UnityEngine;
using System.Collections;

public class Complication : ScriptableObject
{

    [SerializeField]
    LevelParameters[] Levels;

    public LevelParameters GetLevel()
    {
        if (Levels.Length == 0) return null;
        if (Levels.Length == 1) return Levels[0];

        float[] chances = new float[Levels.Length];

        float max_val = 0;
        int index_max = 0;
        for (int i = 0; i < chances.Length; i++)
        {
            chances[i] = Random.Range(0f, Levels[i].Chance);
            if (chances[i] > max_val)
            {
                max_val = chances[i];
                index_max = i;
            }
        }

        return Levels[index_max];
    }

}
