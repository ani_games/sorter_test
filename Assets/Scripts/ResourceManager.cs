﻿using UnityEngine;
using System.Collections;

public class ResourceManager
{
    PathManager pathManager;
    public PathManager PathManager{ get { return pathManager; } }

    public ResourceManager()
    {
        pathManager = Resources.Load<PathManager>("Parameters/PathManager");
    }
	
}
