﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Collector : MonoBehaviour
{

    object lockObject = new object();

    Dictionary<string, CollectPlace> places;

    CollectPlace[] PlacePoint = { CollectPlace.A, CollectPlace.B, CollectPlace.C, CollectPlace.D };

    [SerializeField]
    Vector3[] PointPositions;

    [SerializeField]
    Vector3[] MarkerPositions;

    Vector3[] CarRotations = { new Vector3(0,270,0), new Vector3(0,90,0), new Vector3(0,270,0), new Vector3(0,90,0) };

    [SerializeField]
    GameObject carPrefab;

    List<GameObject> GameObjects;

    Dictionary<CollectPlace, GameObject> Markers;

    Dictionary<CollectPlace, GameObject> Cars;

    void Awake()
    {
        places = new Dictionary<string, CollectPlace>();
        GameObjects = new List<GameObject>();
    }


    public void Set(BoxParameters[] boxParams)
    {
        places.Clear();

        for (int i = 0; i < boxParams.Length; i++)
        {
            for (int j = 0; j < boxParams.Length - 1; j++)
            {
                if (Random.Range(0, 2) == 0)
                {
                    BoxParameters tmp = boxParams[j];
                    boxParams[j] = boxParams[j + 1];
                    boxParams[j + 1] = tmp;
                }
            }
        }

        Markers = new Dictionary<CollectPlace, GameObject>();
        Cars = new Dictionary<CollectPlace, GameObject>();

        StopAllCoroutines();

        for (int i = 0; i < boxParams.Length; i++)
        {
            places.Add(boxParams[i].UniqueID, PlacePoint[i]);

            StartCoroutine(NewCar(PlacePoint[i], boxParams[i]));

        }


    }


    IEnumerator NewCar(CollectPlace place, BoxParameters par)
    {

        GameObject car = Instantiate(carPrefab, PointPositions[(int)place], Quaternion.identity) as GameObject;

        car.transform.localRotation = Quaternion.Euler(CarRotations[(int)place]);
        car.GetComponent<Animator>().Play("car_in");

        GameObjects.Add(car);
        Cars.Add(place, car);
        AddMarker(car, par, place);

        yield return new WaitForSeconds(1.5f);

        
        
    }

    void AddMarker(GameObject car, BoxParameters par, CollectPlace place)
    {
        GameObject marker = Instantiate(par.Prefab) as GameObject;
        marker.transform.parent = car.transform;
        marker.transform.localPosition = MarkerPositions[(int)place];
        marker.transform.rotation = Quaternion.identity;

        Markers.Add(place, marker);
        GameObjects.Add(Markers[place]);
    }

    public CollectPlace GetEmpty()
    {
        List<CollectPlace> emptiesPlaces = new List<CollectPlace>();

        foreach (CollectPlace cp in System.Enum.GetValues(typeof(CollectPlace)))
        {
            if (!places.ContainsValue(cp)) emptiesPlaces.Add(cp);
        }

        if (emptiesPlaces.Count == 0)
            return CollectPlace.None;
        else
            return emptiesPlaces[Random.Range(0, emptiesPlaces.Count)];
    }

    public ControlDirection GetDirection(string id)
    {
        if (places.ContainsKey(id))
        {
            CollectPlace place = places[id];
            switch (place)
            {
                case CollectPlace.A:
                case CollectPlace.C:
                    return ControlDirection.Left;
                case CollectPlace.B:
                case CollectPlace.D:
                    return ControlDirection.Right;
            }
        }

        return ControlDirection.None;
    }

    public CollectPlace GetPlace(string id)
    {

        if(places.ContainsKey(id))return places[id];

        return CollectPlace.None;
    }

    void OnDestroy()
    {
        
        GameController.Instance.Loader.UnLoad(GameObjects);
        List<GameObject> markers = new List<GameObject>();
        foreach (GameObject go in Markers.Values)
        {
            markers.Add(go);
        }
        GameController.Instance.Loader.UnLoad(markers);
        GameObjects.Clear();
    }

    public void Clear()
    {
        StartCoroutine(EndRound());
    }

    IEnumerator EndRound()
    {
        foreach (GameObject marker in Markers.Values)
        {
            Destroy(marker);
        }

        foreach (GameObject car in Cars.Values)
        {
            car.GetComponent<Animator>().Play("car_out");
        }
        yield return new WaitForSeconds(1.5f);
        OnDestroy();
    }

    public void ChangePlace(string oldId, string newId, BoxParameters box )
    {
        CollectPlace pl = GetPlace(oldId);
        places.Remove(oldId);
        places.Add(newId, pl);

        StartCoroutine(CarChange(pl,box));
    }

    IEnumerator CarChange(CollectPlace pl, BoxParameters box)
    {
        lock(lockObject)
        {
            if (!places.ContainsValue(pl)) yield break;

            GameObject garbage = Markers[pl];
            Markers.Remove(pl);
            Destroy(garbage);
            
            Cars[pl].GetComponent<Animator>().Play("car_out");
            
            

            if (places.Count == 4)
            {
                AddMarker(Cars[pl], box, pl);
                yield return new WaitForSeconds(1.5f);
                Cars[pl].GetComponent<Animator>().Play("car_in");
            }
            else
            {
                CollectPlace newPlace = CollectPlace.None;
                foreach (CollectPlace cp in System.Enum.GetValues(typeof(CollectPlace)))
                {
                    if (!places.ContainsValue(cp))
                    {
                        newPlace = cp;
                        break;
                    }
                }

                places.Remove(box.UniqueID);
                places.Add(box.UniqueID, newPlace);

                Destroy(Cars[pl]);
                GameObjects.Remove(Cars[pl]);
                Cars.Remove(pl);

                StartCoroutine(NewCar(newPlace, box));

            }
        }

               
        
    }

}
