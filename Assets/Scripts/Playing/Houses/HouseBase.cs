﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HouseBase : MonoBehaviour
{

    [SerializeField]
    Light Sun;

    HouseSettings settings;
    Passport passport;
    LevelParameters CurrentLevel;

    float TransporterSpeed;
    float appearTimeout;

    BoxParameters[] boxParams;
    List<GameObject> boxes;
    List<GameObject> tempObjects = new List<GameObject>();

    CollectStatistic statistic;

    Mover mover;
    Collector collector;
    

    public void Set(HouseSettings settings, Passport passport)
    {
        Sun.intensity = settings.LightIntensity;

        this.passport = passport;
        this.settings = settings;

        SetNewLevel();

        Control.ControlAction += Pick;
        Mover.OnSkip += SkipBox;
    }

    void SetNewLevel()
    {
        StopAllCoroutines();

        GameController.Instance.Loader.UnLoad(tempObjects);

        CurrentLevel = passport.Complication.GetLevel();

        boxParams = SelectBoxParams( CurrentLevel.Boxes );

        TransporterSpeed = Mathf.Clamp(CurrentLevel.TransporterSpeed, 0.01f, 1f);

        float time_to_move = ((settings.BoxEndPosition - settings.BoxStartPosition).magnitude / TransporterSpeed) * Time.fixedDeltaTime;
        int howManyBoxesInTransporter = (int)((settings.BoxEndPosition - settings.BoxStartPosition).magnitude / (CurrentLevel.BoxScale + 0.5f/*Пол-Юнита добавим, чтоб коробки не смыкались*/));

        appearTimeout = time_to_move / Mathf.Clamp((float)howManyBoxesInTransporter * CurrentLevel.Frequency, 1, howManyBoxesInTransporter);

        boxes = new List<GameObject>();

        PoolBox.Instance.Set(CurrentLevel.Boxes);

        statistic = new CollectStatistic(CurrentLevel.Boxes, CurrentLevel.MaxCount);

        mover = this.GetComponent<Mover>();
        mover.Set(settings, boxes, TransporterSpeed, boxParams);

        collector = this.GetComponent<Collector>();
        collector.Set(boxParams);

        StartCoroutine(SpownBoxes());
    }

    void Lose()
    {
        GameController.Instance.State = GameState.Menu;
    }

    void SkipBox(string id, bool isExistCollector)
    {
        StartCoroutine(BoxFly(0, id, ControlDirection.None, isExistCollector));
    }

    IEnumerator SpownBoxes()
    {
        while (true)
        {
            BoxParameters par = GetBoxParam();
            if (par != null)
            {
                GameObject go = PoolBox.Instance.GetBox(par.UniqueID);
                go.SetActive(true);
                go.transform.position = settings.BoxStartPosition;
                go.transform.Rotate(Vector3.up * Random.Range(-15f, 15f));
                go.GetComponent<BoxBase>().Set(settings, TransporterSpeed, par.UniqueID,par);
                boxes.Add(go);
                statistic.Spown(par.UniqueID);
            }
            yield return new WaitForSeconds(appearTimeout+Random.Range(-1*appearTimeout*0.5f,appearTimeout*0.5f));
        }
    }

    

    BoxParameters GetBoxParam()
    {
        if (boxParams.Length == 0 || statistic.isFull())
            return null;

        if (CurrentLevel.Boxes.Length > 4 && Random.Range(0, 5) == 0)
        {
            List<BoxParameters> pars = new List<BoxParameters>();
            for (int i = 0; i < CurrentLevel.Boxes.Length; i++)
            {
                bool contains = false;
                for (int j = 0; j < boxParams.Length; j++)
                {
                    if (CurrentLevel.Boxes[i] == boxParams[j])
                    {
                        contains = true;
                        break;
                    }
                }

                if (!contains)
                    pars.Add(CurrentLevel.Boxes[i]);

            }
            BoxParameters p = pars[Random.Range(0, pars.Count)];
            if (p != null)
            {
                statistic.Decremenate(p.UniqueID);
                return p;
            }
        }
        //if (boxParams.Length == 1) return boxParams[0];

        float[] chances = new float[boxParams.Length];

        float max_val = 0;
        int index_max = 0;
        for (int i = 0; i < chances.Length; i++)
        {
            if (!statistic.CanSpown(boxParams[i].UniqueID))//Если волна закончилась или лимит кубов исчерпан, то не спавним больше эти коробки, во всяком случае до новой волны
                continue;

            chances[i] = Random.Range(0f, boxParams[i].Chance);
            if (chances[i] > max_val)
            {
                max_val = chances[i];
                index_max = i;
            }
        }

        if (max_val == 0) return null;
        return boxParams[index_max];
    }

    public void Destroy()
    {
        if(PoolBox.Instance!=null)PoolBox.Instance.Clear();
        GameController.Instance.Loader.UnLoad(boxes);
        GameController.Instance.Loader.UnLoad(tempObjects);
        Debug.Log("Destroy All Objects in da Hous");
    }

    void OnDestroy()
    {
        Control.ControlAction -= Pick;
        Mover.OnSkip-= SkipBox;
        Destroy();
    }

    void Pick(ControlDirection direction)
    {

        if (boxes.Count == 0) return;

        if ((boxes[0].transform.position - settings.BoxStartPosition).magnitude >= settings.StartCollectDistance)
        {
            string id = boxes[0].GetComponent<BoxBase>().ID;

            ControlDirection targetDirection = collector.GetDirection(id);

            if (boxes.Count == 0) return;

            int result = -1;
            if (direction == targetDirection)
            {
                statistic.Collect(id);
                result = 1;
            }
            StartCoroutine(BoxFly(result, id, direction,true));
            
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="result">0 - пропуск, -1 - неверно, 1 - верно</param>
    /// <returns></returns>
    IEnumerator BoxFly(int result, string id, ControlDirection direction, bool isExistCollector)
    {

        GameObject tmp = boxes[0];
        tempObjects.Add(tmp);
        boxes.RemoveAt(0);

        switch (result)
        {
            case -1:
                if(direction==ControlDirection.Left)
                    tmp.GetComponent<BoxBase>().Animate(BoxBase.AnimationType.IncorrectLeft);
                else
                    tmp.GetComponent<BoxBase>().Animate(BoxBase.AnimationType.IncorrectRight);
                break;

            case 0:
                tmp.GetComponent<BoxBase>().Animate(BoxBase.AnimationType.Loos);
                break;

            case 1:
                CollectPlace place = collector.GetPlace(id);

                switch (place)
                {
                    case CollectPlace.A:
                        tmp.GetComponent<BoxBase>().Animate(BoxBase.AnimationType.CorrectA);
                        break;
                    case CollectPlace.B:
                        tmp.GetComponent<BoxBase>().Animate(BoxBase.AnimationType.CorrectB);
                        break;
                    case CollectPlace.C:
                        tmp.GetComponent<BoxBase>().Animate(BoxBase.AnimationType.CorrectC);
                        break;
                    case CollectPlace.D:
                        tmp.GetComponent<BoxBase>().Animate(BoxBase.AnimationType.CorrectD);
                        break;
                }

                break;

        }        

        yield return new WaitForSeconds(1.5f);

        PoolBox.Instance.ReturnToPool(id, tmp);
        tempObjects.Remove(tmp);

        switch (result)
        {
            case 1:                       
                if (statistic.isFull())
                {
                    yield return new WaitForSeconds(1.5f);
                    GameController.Instance.Loader.UnLoad(boxes);
                    boxes.Clear();
                    collector.Clear();
                    PoolBox.Instance.Clear();
                    yield return new WaitForSeconds(1.5f);
                    SetNewLevel();

                }
                else
                {
                    if (statistic.GetCollectedBoxesInWave(id) == statistic.GetBoxWaveCount(id))
                    {
                        yield return new WaitForSeconds(1.5f);
                        CompleteWave(id, boxParams);
                    }
                }                
                break;
            case 0:
                if(isExistCollector)
                    Lose();
                break;
            case -1:
                Lose();
                break;

        }

    }

    BoxParameters[] SelectBoxParams(BoxParameters[] oldParams)
    {
        if (oldParams.Length <= 4)
        {
            BoxParameters[] nParams = new BoxParameters[oldParams.Length];
            for (int i = 0; i < nParams.Length; i++)
            {
                nParams[i] = oldParams[i];
            }
            return nParams;
        }

        float[] chances = new float[oldParams.Length];
        for (int i = 0; i < chances.Length; i++)
        {
            chances[i] = Random.Range(0, oldParams[i].Chance); //TODO Сделать отдельную характеристику шанс
        }

        //сортировка. пузырёк, пузырёк =)
        for (int j = 0; j < chances.Length; j++)
        {
            for (int i = 1; i < chances.Length; i++)
            {
                if (chances[i] > chances[i - 1])
                {
                    float fTmp = chances[i - 1];
                    chances[i-1] = chances[i];
                    chances[i] = fTmp;

                    BoxParameters bTmp = oldParams[i - 1];
                    oldParams[i - 1] = oldParams[i];
                    oldParams[i] = bTmp;

                }
            }
        }

        BoxParameters[] newParams = new BoxParameters[4];

        for (int i = 0; i < newParams.Length; i++)
        {
            newParams[i] = oldParams[i];
        }

        return newParams;

    }

    void CompleteWave(string completeWaveID, BoxParameters[] oldParameters)
    {

        statistic.NewWave(completeWaveID);

        if (CurrentLevel.Boxes.Length <= 4)
        {
            int change_index = 0;
            for (int i = 0; i < oldParameters.Length; i++)
            {
                if (oldParameters[i].UniqueID == completeWaveID)
                    change_index = i;
            }
            collector.ChangePlace(completeWaveID, oldParameters[change_index].UniqueID, oldParameters[change_index]);
        }
        else
        {
            BoxParameters[] commonParams = CurrentLevel.Boxes;

            List<BoxParameters> remPars = new List<BoxParameters>();

            int change_index = 0;
            for (int i = 0; i < oldParameters.Length; i++)
            {
                if (oldParameters[i].UniqueID == completeWaveID)
                    change_index = i;
            }

            for (int i = 0; i < commonParams.Length; i++)
            {
                if (commonParams[i].UniqueID == completeWaveID) continue;
                bool isUsed = false;
                for (int j = 0; j < oldParameters.Length; j++)
                {
                    if (oldParameters[j].UniqueID == commonParams[i].UniqueID)
                        isUsed = true;
                }

                if (!isUsed)
                {
                    if (statistic.GetTotalCollectedBoxes(commonParams[i].UniqueID) < statistic.GetBoxMaxCount(commonParams[i].UniqueID))
                        remPars.Add(commonParams[i]);
                }

            }

            if (remPars.Count > 0)
            {
                oldParameters[change_index] = remPars[Random.Range(0, remPars.Count)];//TODO
                collector.ChangePlace(completeWaveID, oldParameters[change_index].UniqueID, oldParameters[change_index]);
            }



        }

    }

}
