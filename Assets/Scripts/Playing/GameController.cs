﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{

    [SerializeField]
    public Loader Loader;

    ResourceManager resourceManager = null;
    public ResourceManager ResourceManager { get { return resourceManager; } }

    Passport passport = null;
    public Passport Passport { get { return passport; } }

    public GameRound GameRound;

    private GameState _state = GameState.None;
    public GameState State
    {
        get { return _state; }
        set
        {
            if (value == _state)
            {
                Debug.LogError("New Game State is equal of Old State");
                return;
            }
            _state = value;

            switch (value)
            {
                case GameState.None:
                    break;
                case GameState.Menu:
                    GameRound.Destroy();//Это обязательно! Здесь уничтожаются все объекты со сцены и выгружаются неиспользуемые ассеты
                    GameRound = null;
                    break;
                case GameState.Result:
                    break;
                case GameState.StartRound:
                    //Загрузка нового уровня
                    GameRound = new GameRound(passport);
                    break;
            }


        }
    }

   

	void Awake ()
    {
        #region Singleton
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (_instance != this)
                DestroyImmediate(this.gameObject);
        }
        #endregion

        resourceManager = new ResourceManager();
        passport = new Passport("player");//TODO id selection
    }

    #region Singleton
    private static GameController _instance = null;
    public static GameController Instance
    {
        get { return _instance; }
    }
    #endregion

}
