﻿using UnityEngine;
using System.Collections;

public class RoundScore
{

    public RoundScore()
    {
        Points = 0;
    }

    public int Points { get; private set; }

}
