﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectStatistic
{

    Dictionary<string, int> BoxSpowned;
    Dictionary<string, int> BoxWaveSpowned;
    Dictionary<string, int> BoxCollected;
    Dictionary<string, int> BoxWaveCollected;
    Dictionary<string, int> BoxCount;
    Dictionary<string, int> BoxWaveCount;

    int AllBoxes;
    int AllBoxesSpowned;
    int MaxCount;

    public CollectStatistic(BoxParameters[] boxParams, int MaxCount)
    {
        AllBoxes = 0;
        AllBoxesSpowned = 0;
        this.MaxCount = MaxCount;

        BoxSpowned = new Dictionary<string, int>();
        BoxWaveSpowned = new Dictionary<string, int>();
        BoxCollected = new Dictionary<string, int>();
        BoxCount = new Dictionary<string, int>();
        BoxWaveCount = new Dictionary<string, int>();
        BoxWaveCollected = new Dictionary<string, int>();

        float count_sum = 0;

        for (int i = 0; i < boxParams.Length; i++)
        {
            BoxSpowned.Add(boxParams[i].UniqueID, 0);
            BoxWaveSpowned.Add(boxParams[i].UniqueID, 0);
            BoxCollected.Add(boxParams[i].UniqueID, 0);
            BoxWaveCollected.Add(boxParams[i].UniqueID, 0);

            count_sum += boxParams[i].FillRate;
        }

        float k = 1.0f / count_sum;

        for (int i = 0; i < boxParams.Length; i++)
        {
            int count = (int)(boxParams[i].FillRate * k * MaxCount+2);
            BoxCount.Add(boxParams[i].UniqueID, count);
            BoxWaveCount.Add(boxParams[i].UniqueID, (int)(count / boxParams[i].Attempt)+1);
        }

    }

    public void Collect(string id)
    {
        BoxCollected[id] += 1;//.Add(id, BoxCollected[id] + 1);
        BoxWaveCollected[id] += 1;//.Add(id, BoxWaveCollected[id] + 1);
        AllBoxes++;
    }

    public void Spown(string id)
    {
        BoxSpowned[id]++;
        BoxWaveSpowned[id]++;
        AllBoxesSpowned++;
    }

    /// <summary>
    /// Используется для выравнивания общего числа коробок, так как любая выпущенная коробка увеличивает счетчики
    /// </summary>
    /// <param name="id"></param>
    public void Decremenate(string id)
    {
        BoxSpowned[id]--;
        BoxWaveSpowned[id]--;
        AllBoxesSpowned--;
    }

    public void NewWave(string id)
    {
        BoxWaveSpowned[id] = 0;
    }

    public bool CanSpown(string id)
    {
        
        if (BoxSpowned[id] < BoxCount[id] && BoxWaveSpowned[id] < BoxWaveCount[id] && AllBoxesSpowned < MaxCount) return true;
        return false;
    }

    public bool isFull()
    {
        Debug.Log("" + AllBoxes + "/" + MaxCount);
        if (AllBoxes >= MaxCount) return true;
        return false;
    }

    public int GetSpownedBoxes(string id)
    {
        return BoxSpowned[id];
    }

    public int GetWaveSpownedBoxes(string id)
    {
        return BoxWaveSpowned[id];
    }

    public int GetCollectedBoxesInWave(string id)
    {
        return BoxWaveCollected[id];
    }

    public int GetTotalCollectedBoxes(string id)
    {
        return BoxCollected[id];
    }

    public int GetBoxWaveCount(string id)
    {
        return BoxWaveCount[id];
    }

    public int GetBoxMaxCount(string id)
    {
        return BoxCount[id];
    }

}
