﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Mover : MonoBehaviour
{
    Vector3 startPosition;
    Vector3 moveVector;
    float failDistance;
    List<GameObject> boxes;
    float speed;

    public static event System.Action<string,bool> OnSkip;

    BoxParameters[] boxParams;

    public void Set (HouseSettings settings, List<GameObject> boxes, float speed, BoxParameters[] pars)
    {
        moveVector = (settings.BoxEndPosition - settings.BoxStartPosition).normalized;
        failDistance = (settings.BoxEndPosition - settings.BoxStartPosition).magnitude;
        this.boxes = boxes;
        this.speed = speed;
        boxParams = pars;
        startPosition = settings.BoxStartPosition;
    }
	
	void Update ()
    {
        for (int i = 0; i < boxes.Count; i++)
        {
            boxes[i].transform.position += moveVector * speed * Time.deltaTime / Time.fixedDeltaTime;

            if ((boxes[i].transform.position - startPosition).magnitude > failDistance)
            {
                if (CheckToCollector(boxes[i].GetComponent<BoxBase>().Parameter))
                {
                    //boxes[i].transform.rotation = Quaternion.identity;
                    if (OnSkip != null)
                        OnSkip(boxes[i].GetComponent<BoxBase>().ID,true);
                }
                else
                {
                    if (OnSkip != null)
                        OnSkip(boxes[i].GetComponent<BoxBase>().ID, false);
                }
            }
        }
    }

    public bool CheckToCollector(BoxParameters par)
    {
        for (int i = 0; i < boxParams.Length; i++)
        {
            if (par == boxParams[i])
                return true;
        }

        return false;
    }
}
