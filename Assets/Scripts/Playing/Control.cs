﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour
{

    public static event System.Action<ControlDirection> ControlAction;

    void Update ()
    {
        if (Input.GetMouseButtonDown(0) && ControlAction!=null )
        {
            if (Input.mousePosition.x > Screen.width * 0.5f)
                ControlAction(ControlDirection.Right);
            else
                ControlAction(ControlDirection.Left);
        }
	}
}
