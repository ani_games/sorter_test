﻿using UnityEngine;
using System.Collections;

public class Passport
{

    private string uniqueID;
    private string housePath;
    private int totalScore;
    private int bestScore;

    public string UniqueID { get { return uniqueID; } }
    public string HousePath { get { return housePath; } }
    public int TotalScore { get { return totalScore; } }
    public int BestScore { get { return bestScore; } }

    private static string total_score_key = "TOTAL_SCORE_";
    private static string best_score_key = "BEST_SCORE_";

    public Complication Complication;

    public Passport(string id)
    {
        LoadPassport(id);
    }

    /// <summary>
    /// Load passport data from storage
    /// </summary>
    /// <param name="id">Unique id of player</param>
    public void LoadPassport(string id)
    {
        uniqueID = id;
        housePath = GameController.Instance.ResourceManager.PathManager.HousesPath + "/" + id;
        totalScore = PlayerPrefs.GetInt(total_score_key + id);
        bestScore = PlayerPrefs.GetInt(best_score_key + id);

        Complication = Resources.Load<Complication>(GameController.Instance.ResourceManager.PathManager.ComplicationPath + uniqueID);
    }

    /// <summary>
    /// Update passport data and save.
    /// </summary>
    /// <param name="score">Current game round score.</param>
    public void UpdatePassport(RoundScore score)
    {
        totalScore += score.Points;
        if (score.Points > bestScore) bestScore = score.Points;
        SaveThisPassport();
    }

    /// <summary>
    /// Save current passport data.
    /// </summary>
    public void SaveThisPassport()
    {
        PlayerPrefs.SetInt(total_score_key + uniqueID, totalScore);
        PlayerPrefs.SetInt(best_score_key + uniqueID, bestScore);
    }

    /// <summary>
    /// Return true if current score more then best record.
    /// </summary>
    /// <param name="score">Current game round score.</param>
    /// <returns></returns>
    public bool IsNewBestScore(RoundScore score)
    {
        return score.Points > bestScore;
    }

    public override string ToString()
    {
        if (this != null)
            return "Passport of " + uniqueID + ".\n\tHouse Path: " + housePath + "\n\tTotal Score: " + totalScore + "\n\tBest Score: " + bestScore;
        else
            return "Pasport Not Initialized!";
    }


}
