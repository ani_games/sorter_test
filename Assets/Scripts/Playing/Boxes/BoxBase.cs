﻿using UnityEngine;
using System.Collections;

public class BoxBase : MonoBehaviour
{
    [HideInInspector]
    public string ID = "";

    [SerializeField]
    Animator animator;

    Vector3 collectPoint;

    public BoxParameters Parameter; 

    public void Set(HouseSettings settings, float speed, string id, BoxParameters pars)
    {
        ID = id;
        animator = GetComponent<Animator>();
        collectPoint = settings.BoxEndPosition;
        Parameter = pars;
    }
	
	void Start ()
    {
	
	}

    public void Lose()
    {

    }

    public void Animate(AnimationType type)
    {
        this.transform.rotation = Quaternion.identity;
        this.transform.position = collectPoint;

        switch (type)
        {
            case AnimationType.Loos:
                animator.Play("box_lose");
                break;
            case AnimationType.IncorrectLeft:
                animator.Play("box_incorrect_left");
                break;
            case AnimationType.IncorrectRight:
                animator.Play("box_incorrect_right");
                break;
            case AnimationType.CorrectA:
                animator.Play("box_collect_a");
                break;
            case AnimationType.CorrectB:
                animator.Play("box_collect_b");
                break;
            case AnimationType.CorrectC:
                animator.Play("box_collect_c");
                break;
            case AnimationType.CorrectD:
                animator.Play("box_collect_d");
                break;
        }
    }

    public enum AnimationType
    {
        Loos,
        IncorrectLeft,
        IncorrectRight,
        CorrectA,
        CorrectB,
        CorrectC,
        CorrectD
    }
}
