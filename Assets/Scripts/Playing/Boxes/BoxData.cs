﻿using UnityEngine;
using System.Collections;

namespace SorterINC.Boxes
{
    public class BoxData
    {
        public string UniqueID;
        public GameObject Obj;

        public BoxData(string id, GameObject obj)
        {
            UniqueID = id;
            Obj = obj;
        }

    }
}