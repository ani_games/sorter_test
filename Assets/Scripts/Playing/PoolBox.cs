﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SorterINC.Boxes;

public class PoolBox : MonoBehaviour
{

    private static PoolBox _instance = null;
    public static PoolBox Instance { get { return _instance; } }

    //private Dictionary<string, GameObject> templates;

    List<BoxData> pool;

    Vector3 appearPos = new Vector3(0, 100, -100);

	void Awake ()
    {
        _instance = this;
        pool = new List<BoxData>();
        //templates = new Dictionary<string, GameObject>();
	}

    public void Set(BoxParameters[] boxParams)
    {
        //templates.Clear();

        for (int i = 0; i < boxParams.Length; i++)
        {
            if (boxParams[i].UniqueID.Equals(string.Empty))
            {
                Debug.LogError("Box Unique ID is Empty!");
                Debug.Break();
                break;
            }
            GameObject go = Instantiate(boxParams[i].Prefab, appearPos, Quaternion.identity) as GameObject;
            go.SetActive(false);
            go.transform.parent = this.transform;
            go.name = boxParams[i].UniqueID;
            pool.Add(new BoxData(boxParams[i].UniqueID, go));
            DublicateBox(boxParams[i].UniqueID);
            DublicateBox(boxParams[i].UniqueID);
        }

    }

    void DublicateBox(string id)
    {
        for (int i = 0; i < pool.Count; i++)
        {
            if (pool[i].UniqueID == id)
            {
                GameObject go = Instantiate(pool[i].Obj, appearPos, Quaternion.identity) as GameObject;
                go.transform.parent = this.transform;
                go.name = id;
                pool.Add(new BoxData(id, go));
                break;
            }
        }
    }

    public GameObject GetBox(string id)
    {
        bool haveOneInstance = false;//будем проверять. Нужно чтобы всегда оставался хоть один экземпляр с данным ИД
        GameObject template = null;
        for (int i = 0; i < pool.Count; i++)
        {
            if (pool[i].UniqueID == id)
            {
                if (!haveOneInstance)
                {
                    haveOneInstance = true;//Проверили, один экземпляр есть, следующий можем отдать
                    template = pool[i].Obj;
                }
                else
                {
                    GameObject go = pool[i].Obj;
                    pool.RemoveAt(i);
                    go.SetActive(true);
                    return go;
                }
            }
        }
        GameObject ngo = Instantiate(template, appearPos, Quaternion.identity) as GameObject;
        ngo.transform.parent = this.transform;
        ngo.name = id;
        return ngo;
    }

    public void ReturnToPool(string id, GameObject obj)
    {
        obj.transform.position = appearPos;
        obj.SetActive(false);
        pool.Add(new BoxData(id, obj));
    }

    public void Clear()
    {
        List<GameObject> removeList = new List<GameObject>();
        foreach (BoxData data in pool)
        {
            removeList.Add(data.Obj);
        }
        pool.Clear();
        GameController.Instance.Loader.UnLoad(removeList);
    }

}
