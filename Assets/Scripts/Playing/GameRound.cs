﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameRound
{

    RoundScore score;

    GameObject houseObject;

    Loader loader;

    HouseSettings settings;

    HouseBase HouseBase;

    public GameRound(Passport passport)
    {

        score = new RoundScore();

        loader = GameController.Instance.Loader;

        settings = Resources.Load<HouseSettings>(GameController.Instance.ResourceManager.PathManager.HousesSettingsPath + passport.UniqueID);

        houseObject = loader.Load(passport.HousePath);
        HouseBase = houseObject.GetComponent<HouseBase>();
                
        HouseBase.Set(settings, passport);
    }

    public void Destroy()
    {
        loader.UnLoad(houseObject);
    }

    public void OnDestroy()
    {
        Destroy();
    }

}
