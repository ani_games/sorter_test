﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Loader : MonoBehaviour
{

    public GameObject Load(string path)
    {
        GameObject go = Resources.Load<GameObject>(path);
        GameObject obj = Instantiate(go);
        return obj;
    }

    public void UnLoad(List<GameObject> values)
    {
        foreach (GameObject go in values)
        {
            Destroy(go);
        }
        Resources.UnloadUnusedAssets();
    }

    public void UnLoad(GameObject go)
    {
        Destroy(go);        
        Resources.UnloadUnusedAssets();
    }

}
