﻿using UnityEngine;
using System.Collections;

public class UICommon : MonoBehaviour
{

    public void ShowLeaderbord()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        MyGPGServices.ShowLeaderboard();
#endif

    }

    public void ShowAchievmentsUI()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
         MyGPGServices.ShowAchievments();
#endif

    }

}
